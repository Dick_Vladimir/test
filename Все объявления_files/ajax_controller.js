$.fn.serializeObject = function(){

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };


    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){

        // skip invalid keys
        if(!patterns.validate.test(this.name)){
            return;
        }

        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            // push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }

            // named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }

        json = $.extend(true, json, merge);
    });

    return json;
};

$.extend({
    getUrlVars: function(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    }
});

var processing = false;
var page_active = false;
var cryts = {
    current_path: '/',
    category_id: false,
    category_get_param: false,
    filter: {},
    ff: {},
    skip_title: false,
    searchAllText: '',
    page: 1,
    per_page: 24,
    owner_type: 'all',
    owner: (undefined != owner_id)  ? owner_id : false,
    disable_history: false,
    sort_field: 'date_public_start',
    sort_dir: 'desc',
    currency: 'RUR',
    updated: false,
    only_manual: false,
    hide_empty: false,
    check_auth: true
};
var subtemplate;

var first_render = true;
var by_history = false;

function convertDate(getData) {

    var now = new Date();

    var day = parseInt(/^(\d{2})\./.exec(getData)[1]);
    var month = parseInt(/\.(\d{2})\./.exec(getData)[1]);
    var year = /(\d{4})\s/.exec(getData)[1];
    var time = /\d{4}\s(\d+:\d+)$/.exec(getData);

    var nYear = now.getFullYear();
    var nMonth = now.getMonth()+1;
    var nDay = now.getDate();

    var yesterDate = new Date(now - 24*60*60*1000);
    var corrDtime = day +'.'+ month + '.' + year + ' в ' + time[1];

    if (day == nDay && month == nMonth && year == nYear)
        corrDtime = 'Сегодня в ' + time[1];
    if (yesterDate.getDate() == day && month == nMonth && year == nYear){
        corrDtime = 'Вчера в ' + time[1];
    }

    return corrDtime;
}

function convertPrice(price) {
    var currency = cources[localStorage.getItem('currency') ? localStorage.getItem('currency') : 'RUR'];
    return (currency.prefix ? currency.prefix + ' ' : '') + (price / currency.rate).toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + (currency.suffix ? ' ' + currency.suffix : '');
}

function toggleManuals() {
    cryts.only_manual = !cryts.only_manual;
    $("#main_menu").mainMenu('dropCounts');
    getPageContent();
}

function setSettingsBtnClass () {
    $("button[data-target='#settings']").removeClass('btn-primary');
    var filter = localStorage.getItem('filter') ? JSON.parse(localStorage.getItem('filter')) : {};
    _.each(filter, function(item, iterator){
        if(undefined != filter[iterator] && filter[iterator] == true)
            $("button[data-target='#settings']").addClass('btn-primary');
    });
    if(localStorage.getItem('owner_type') && localStorage.getItem('owner_type') != 'all')
        $("button[data-target='#settings']").addClass('btn-primary');
    if(localStorage.getItem('per_page') && localStorage.getItem('per_page') != 24)
        $("button[data-target='#settings']").addClass('btn-primary');
    if(localStorage.getItem('currency') && localStorage.getItem('currency') != 'RUR')
        $("button[data-target='#settings']").addClass('btn-primary');
}

function setSortBtnClass () {
    $("button[data-target='#sort_cryts']").removeClass('btn-primary');
    if(localStorage.getItem('sort_field') && localStorage.getItem('sort_field') != 'date_public_start')
        $("button[data-target='#sort_cryts']").addClass('btn-primary');
    if(localStorage.getItem('sort_dir') && localStorage.getItem('sort_dir') != 'desc')
        $("button[data-target='#sort_cryts']").addClass('btn-primary');
}

function setSortCryt (cryt, field, dir) {

    cryts.sort_dir = (cryts.sort_field != field) ? dir : (cryts.sort_dir == 'asc' ? 'desc' : 'asc');
    cryts.sort_field = field;

    $("#sort_cryts .modal-body button .fa-check").css('color', 'transparent');
    $("#sort_cryts .modal-body button .fa-sort-amount-asc, #sort_cryts .modal-body button .fa-sort-amount-desc").remove();
    $(cryt).children('i.fa-check').css('color', '#0F0');
    $(cryt).append('<i class="fa fa-sort-amount-' + cryts.sort_dir + '"></i>');
}

function toggleFavorites(mode) {
    if(undefined != mode) {
        localStorage.setItem('only_favorites', localStorage.getItem('only_favorites') == 1 ? 0 : 1);
        $("#main_menu").mainMenu('dropCounts');
        getPageContent();
    }
    if(localStorage.getItem('only_favorites') == 1)
        $("#fav-mode-btn").addClass('btn-danger').removeClass('btn-primary');
    else
        $("#fav-mode-btn").removeClass('btn-danger').addClass('btn-primary');
}

var adverts_data = {};

function render_list(noscroll) {

    if (cryts.owner_type == 'only_own' && cryts.owner == 204711) {
        subtemplate = 'photo';
    } else {
        subtemplate = ($.cookie('subtemplate') && $.cookie('subtemplate') != 'lent') ? $.cookie('subtemplate') : 'lent';
    }

    $('.content-right').removeClass('st_photo st_lent st_table').addClass('st_' + subtemplate);

    //var subtemplate = localStorage.getItem('subtemplate') ? localStorage.getItem('subtemplate') : 'lent';
    var template = _.template($('#' + subtemplate + '-tmpl').html());

    if(adverts_data.numpages == undefined)
        adverts_data.numpages  = '';

    if($(".similar-block").size()) {
        $(".similar-block").html('');
        $(".similar-block").html(template(adverts_data));
    } else {
        $(".content-right").html('');
        $(".content-right").append(template(adverts_data));
    }

    if(!noscroll)
        $("body").animate({scrollTop: 0}, '100', 'swing');

    mrks_arr = [];
    other_points = adverts_data.other_points ? adverts_data.other_points : [];

    _.each(adverts_data.result, function (item, iterator) {

        if (item.latitude > 0 && item.longitude > 0) {
            mrks_arr.push([
                item.text,
                '',
                item.alt_name,
                parseFloat(item.latitude),
                parseFloat(item.longitude),
                '<img src="' + (item.foto_default.src) + '" />',
                item.top_class,
                item.colored_class,
                item,
                item.map_marked
            ]);
        }
        $('#rating_' + item.id).rating({
                fx: 'float',
                image: '/templates/expert/images/stars.png',
                loader: '/templates/expert/images/ajax-loader.gif',
                stars: 5,
                minimal: 0,
                readOnly: true
            });
    });

    if(subtemplate == 'table')
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'hover'
        });

    //$(".nominal").each(function () {
    //    var str = $(this).html();
    //    $(this).html(str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    //});

    //$(".categories a").click(function () {
    //    by_history = false;
    //    cryts.searchAllText = '';
    //    cryts.ff = {};
    //    if($(this).attr('data-buisness_categories'))
    //        cryts.ff.buisness_categories = $(this).attr('data-buisness_categories');
    //    if($(this).attr('data-business_types'))
    //        cryts.ff.business_types = $(this).attr('data-business_types');
    //    localStorage.setItem('page', 1);
    //    localStorage.setItem('url', $(this).attr("href"));
    //    getPageContent($(this).attr("href"));
    //    return false;
    //});

    $(".content-left, .content-right, .bottom-panel").removeAttr('style');
    $(".logo_block-bottom .col-sm-12").removeClass("col-sm-12").addClass("col-sm-6");

    if(typeof resize_adverts_block == 'function') {
        $(window).resize(resize_adverts_block);
        resize_adverts_block();
    }


    if(typeof prepareMarkers == "function")
        prepareMarkers();

    if(localStorage.getItem('show_map') && localStorage.getItem('show_map') != 0) {
        $("#map").slideDown(500, function(){LoadMap()});
        $("#map_btn").addClass('btn-primary').removeClass('btn-gray');
    }

    var current_offset = 600;
    if ($(".content-right").length) {
        current_offset = current_offset + $(".content-right").offset().top;
    } else if ($(".similar-block").length) {
        current_offset = current_offset + $(".similar-block").offset().top;
    }

    $("img.lazy").lazyload({
        threshold: current_offset,
        skip_invisible : false,
        placeholder: "/templates/expert/images/no_image-500x500.jpg",
        effect: "fadeIn",
        effectTime: 600
    });

    if(localStorage.getItem('map_fullscreen') && localStorage.getItem('map_fullscreen') == 1) {
        $("img.lazy").lazyload({skip_invisible : true, threshold: 1000000});
    }

    var default_marker_preset = false;

    $(".content-right .advert_lent_view, .content-right .advert_photo_view, .content-right .advert_table_view").on('mouseenter',
        function(){
            if(!yaClusterer)
                return;

            var id = $(this).attr('data-advert-id');
            var geoObjects = yaClusterer.getGeoObjects();
            for(var nn = 0; nn < geoObjects.length; nn++) {
                if(yaClusterer.getGeoObjects()[nn].properties.get('element_id') == id) {
                    var marker = yaClusterer.getGeoObjects()[nn];
                    var markerState =  yaClusterer.getObjectState(marker);

                    if(markerState.isClustered) {
                        markerState.cluster.options.set({preset: 'islands#violetClusterIcons'});
                    } else {
                        default_marker_preset = marker.options.get('preset');
                        marker.options.set({preset: 'islands#violetIcon'});
                    }

                    break;
                }
            }
        }
    );
    $(".content-right .advert_lent_view, .content-right .advert_photo_view, .content-right .advert_table_view").on('mouseleave',
        function(){
            if(!yaClusterer)
                return;

            var id = $(this).attr('data-advert-id');
            var geoObjects = yaClusterer.getGeoObjects();
            for(var nn = 0; nn < geoObjects.length; nn++) {
                if(yaClusterer.getGeoObjects()[nn].properties.get('element_id') == id) {
                    var marker = yaClusterer.getGeoObjects()[nn];
                    var markerState =  yaClusterer.getObjectState(marker);

                    if(markerState.isClustered) {
                        markerState.cluster.options.set({preset: 'islands#blueClusterIcons'});
                    } else {

                        if(default_marker_preset)
                            marker.options.set({preset: default_marker_preset});
                        default_marker_preset = false;
                    }

                    break;
                }
            }
        }
    );
}

function render_empty_list() {
    var empy_data = {
        success: true,
        result: [],
        numpages: ''
    };
    for (var i = 1; i <= localStorage.getItem('per_page'); i++)
        empy_data.result.push({
            id: i,
            is_demo: true,
            alt_name: '',
            colored_class: '',
            top_class: '',
            link: '',
            href: '',
            text: 'Ожидание данных',
            class_top: '',
            phone: null,
            num: i,
            count_fotos: 0,
            views_count: 0,
            count_comments: 0,
            author: 'НЛО',
            parent_category: 'Режим ожидания данных',
            parent_category_link: '',
            description: 'Пожалуйста, подождите. Идет загрузка...',
            is_outdate: '',
            video: null,
            type: 'загрузка',
            foto_default: {
                filename: "logotype.jpg",
                filepath: 'http://dev.prodai.ru/templates/expert/images/logotype.jpg',
                src: '',
                ext: "jpg",
                width: 257,
                height: 193,
                "void:template": "lazy"
            }
        });


    var subtemplate;
    if (cryts.owner_type == 'only_own' && cryts.owner == 204711) {
        subtemplate = 'photo';
    } else {
        subtemplate = ($.cookie('subtemplate') && $.cookie('subtemplate') != 'lent') ? $.cookie('subtemplate') : 'lent';
    }
    //var subtemplate = localStorage.getItem('subtemplate') ? localStorage.getItem('subtemplate') : 'lent';
    var template = _.template($('#' + subtemplate + '-tmpl').html());

    if($(".similar-block").size())
        $(".similar-block").html(template(empy_data));
    else
        $(".content-right").html(template(empy_data));

    if(typeof resize_adverts_block == "function")
        resize_adverts_block();
}

function getPageContent(link) {

    if(processing)
        return false;

    if(!cryts.disable_history && localStorage.getItem('go_back_data') && localStorage.getItem('view_advert')) {

        localStorage.removeItem('go_back');
        localStorage.removeItem('view_advert');
        by_history = false;
        var data = JSON.parse(localStorage.getItem('go_back_data'));

        apply_data(data);
        $("#main_menu").mainMenu('changeUrl', data.link);

        processing = false;

        $('.modal-backdrop, .fade-logo').fadeOut('fast', function(){
            $('.modal-backdrop, .fade-logo').remove();
        });

    } else {

        render_empty_list();

        if(localStorage.getItem('disableSearchString') != 'true') {
            if ($.trim($("#full_text_field").val()) != '') {
                cryts.searchAllText = $("#full_text_field").val();
            } else if ($.trim($("#search_keyword").val()) != '') {
                cryts.searchAllText = $("#search_keyword").val();
            }
        }

        localStorage.setItem('disableSearchString', false);

        if(undefined != link)
            localStorage.setItem('url', link);

        processing = true;

        //if($(".modal-backdrop").size() == 0)
        //    $('body').append('<div class="modal-backdrop fade in"></div><div class="fade-logo" style="background: url(/templates/expert/images/FadeLogo.png) no-repeat center; width: 100%; height: 100%; position: fixed; top: 0; left: 0; z-index: 1000000"></div>');

        if (cryts.owner_type == 'only_own') {
            only_favorites_value = false;
        } else {
            only_favorites_value = localStorage.getItem('only_favorites') == 1 ? true : false;
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: (undefined != cryts.address && undefined !=  cryts.lat && undefined != cryts.lng) ? '/classifieds/getItemListByAddress' : '/classifieds/getAdvertsListJson',
            data: {
                category_path: cryts.category_path != undefined ? cryts.category_path : localStorage.getItem('url'),
                category_id: cryts.owner ? (!cryts.category_id ? $.getUrlVar('category_id') : false) : false,
                p: localStorage.getItem('page') - 1,
                per_page: localStorage.getItem('per_page'),
                i_need_deep: 5,
                use_placement: 1,
                address: cryts.address ? cryts.address : false,
                latitude: cryts.lat ? cryts.lat : false,
                longitude: cryts.lng ? cryts.lng : false,
                only_manual: cryts.only_manual,
                placement: {
                    country: localStorage.getItem('current_country') ? localStorage.getItem('current_country') : false,
                    region: localStorage.getItem('current_region') ? localStorage.getItem('current_region') : false,
                    state: localStorage.getItem('current_state') ? localStorage.getItem('current_state') : false,
                    city: localStorage.getItem('current_city') ? localStorage.getItem('current_city') : false,
                    rajon: localStorage.getItem('current_rajon') ? localStorage.getItem('current_rajon') : false,
                    index: localStorage.getItem('current_index') ? localStorage.getItem('current_index') : false,
                    orientir: localStorage.getItem('current_orientir') ? localStorage.getItem('current_orientir') : false
                },
                use_criteries: 1,
                only_favorites: only_favorites_value,
                //ignore_favorites: 1,
                filter: localStorage.getItem('filter'),
                ff: cryts.ff,
                'search-all-text': cryts.searchAllText,
                sort: {
                    sort_field: localStorage.getItem('sort_field'),
                    sort_dir: localStorage.getItem('sort_dir')
                },
                owner_type: cryts.owner_type,
                owner: (cryts.owner_type == 'only_own') ? cryts.owner : false
                //, request_top_block: ($(".top_banner_container").length > 0) ? 1 : 0
            },
            success: function (response) {

                if($.getUrlVar('category_id'))
                    $("[href='.dealer-page']:last").tab('show');
                cryts.category_path = undefined;

                if(cryts.check_auth && response.need_auth == true) {
                    alert('Для использования опции "Только свои" необходимо авторизоваться!');
                    $(".need_auth").hide();
                }

                if(response.is_auth == true) {
                    $(".need_auth").show();
                } else {
                    $(".need_auth").hide();
                }

                if(response.success == false) {
                    return;
                }

                if(!cryts.disable_history)
                    localStorage.setItem('go_back_data', JSON.stringify(response));
                apply_data(response);

                $('#full_text_field').val('');

                cryts.updated = false;
                response.timestamp = Math.floor(Date.now() / 1000);

                cryts.category_id = response.category_id;
                cryts.current_path = response.link;

                if((cryts.owner && cryts.category_get_param) || cryts.ff.buisness_categories) {
                    if (link == undefined)
                        link = '/';
                    $("#main_menu").mainMenu('changeUrl', response.link);

                    history.replaceState(response, null, window.location.pathname + (
                            response.category_id != 69 ? ('?category_id=' + response.category_id + (
                                response.category_id == 379 ? (
                                    cryts.ff.buisness_categories ? ('&buisness_categories=' + cryts.ff.buisness_categories +
                                        (cryts.ff.business_types ? ('&business_types=' + cryts.ff.business_types) : '')
                                    ) : ''
                                ) : ''
                            )) : ''
                        ));
                } else {
                    if (link == undefined)
                        link = window.location.pathname;

                    history.replaceState(response, null, link);
                    $("#main_menu").mainMenu('changeUrl', link);
                }

                processing = false;

                $('.modal-backdrop, .fade-logo').fadeOut('fast', function(){
                    $('.modal-backdrop, .fade-logo').remove();
                    if (!('ab' in window)) {
                        window.ab = true;
                    }
                    if (window.ab){
                        $('#abModal').modal({
                            keyboard: false
                        });
                    }
                });
            }
        });
    }
};

function apply_data(data) {

    adverts_data = data;
    $(".total_adverts_count").html(data.total);
    $("#sort_cryts .modal-body").html(data.sort);
    if(undefined != data.numpages)
        $(".pagination_block").html(data.numpages);
    else
        $(".pagination_block").html('');

    //if(undefined != data.menu) {
    //    $(".main_menu").remove();
    //    $(".content-left").prepend(data.menu);
    //}

    if(undefined != data.filter) {
        if($(".dealer-page").size()){
            $(".dealer-page .well").remove();
            $(".dealer-page #main_menu").after(data.filter);
        } else {
            $("#side-controls .well").remove();
            $("#side-controls").append(data.filter);
        }
    }

    //if(undefined == data.save_address) {
    cryts.address = data.address;
    cryts.lat = data.lat;
    cryts.lng = data.lng;
    //}

    $("#filter_location").text($(".location_name a").text());

    $(".breadcrumbs").html(data.breadcrumbs);

    if(data.title != undefined && !cryts.skip_title)
        document.title = data.title;

    $(".well .addon-button").bind('click', function(){
        localStorage.setItem('page', 1);
    });

    $("#apply_buttons .btn-primary, #additionalFields .btn-primary").mouseup(function(){

        $("#additionalFields").modal("hide");

        cryts.searchAllText = $("#search_keyword").val();
        var form_data = $("#filter_form").serializeObject();
        var buisness_categories = cryts.ff.buisness_categories,
            business_types = cryts.ff.business_types;
        cryts.ff = undefined != form_data.ff ? form_data.ff : {};
        cryts.ff.buisness_categories = buisness_categories;
        cryts.ff.business_types = business_types;
        //localStorage.setItem('page', 1);
        getPageContent();
        return false;
    });

    $("#apply_buttons .btn-default").click(function(){
        $("#search_keyword").val('');
        cryts.searchAllText = '';
        cryts.ff = {};
        //localStorage.setItem('page', 1);
        getPageContent();
        return false;
    });

    $("#filter_form input[type='text']").keydown(function(e){
        if(e.keyCode == 13) {
            cryts.searchAllText = $("#search_keyword").val();
            var form_data = $("#filter_form").serializeObject();
            var buisness_categories = cryts.ff.buisness_categories,
                business_types = cryts.ff.business_types;
            cryts.ff = undefined != form_data.ff ? form_data.ff : {};
            cryts.ff.buisness_categories = buisness_categories;
            cryts.ff.business_types = business_types;
            localStorage.setItem('page', 1);
            getPageContent();
            return false;
        }
    });

    if(data.favorites_count == 'not_auth') {
        var favorites = $.cookie('favorites') ? JSON.parse($.cookie('favorites')) : [];
        if(favorites.length)
            $(".fa-star").addClass('fav_active');
    } else {
        if(data.favorites_count > 0)
            $(".fa-star").addClass('fav_active');
    }

    //if(data.top_block) {
    //    $(".top_banner_container").append(data.top_block);
    //    $(".top_banner_container .row:last").css('display', 'none');
    //    $(".top_banner_container .row:last img").ready(function(){
    //        $(".top_banner_container .row:first").slideUp(300, function(){
    //            $(".top_banner_container .row:first").remove();
    //        });
    //        $(".top_banner_container .row:last").slideDown(300);
    //    });
    //}

    $(".pagination .current_page").on("blur", function(e){
        getPageContent();
    });

    $(".pagination .current_page").on("keyup", function(e){
        if(e.keyCode == 13)
            getPageContent();
        else
            localStorage.setItem('page', parseInt($(e.target).val()));
    });

    render_list();
    showOwnLink();
}

function initPage() {
    var filter = localStorage.getItem('filter') ? JSON.parse(localStorage.getItem('filter')) : {};

    $.each(filter, function (id, item) {
        if (item) {
            $("#" + id).prop("checked", true);
        }
    });

    localStorage.setItem('owner_type', localStorage.getItem('owner_type') ? localStorage.getItem('owner_type') : cryts.owner_type);
    localStorage.setItem('sort_field', localStorage.getItem('sort_field') ? localStorage.getItem('sort_field') : cryts.sort_field);
    localStorage.setItem('sort_dir', localStorage.getItem('sort_dir') ? localStorage.getItem('sort_dir') : cryts.sort_dir);
    localStorage.setItem('per_page', localStorage.getItem('per_page') ? localStorage.getItem('per_page') : cryts.per_page);
    localStorage.setItem('currency', localStorage.getItem('currency') ? localStorage.getItem('currency') : cryts.currency);

    cryts.owner_type = (cryts.owner_type != 'all') ? cryts.owner_type : localStorage.getItem('owner_type');

    $("#settings input[type='checkbox']").change(function () {
        cryts.filter[$(this).attr('id')] = $(this).is(':checked') ? true : undefined;
    });

    $("#settings input[type='radio'][value='" + localStorage.getItem('owner_type') + "']").prop("checked", true);

    $("#settings input[type='radio']").change(function () {
        cryts.owner_type = $(this).val();
    });

    $("#applyFiltersBtn").click(function () {
        var filter = localStorage.getItem('filter') ? JSON.parse(localStorage.getItem('filter')) : {};
        _.each(cryts.filter, function (item, iterator) {
            filter[iterator] = item;
        });
        localStorage.setItem('filter', JSON.stringify(filter));
        localStorage.setItem('owner_type', cryts.owner_type);
        localStorage.setItem('per_page', cryts.per_page);
        localStorage.setItem('currency', cryts.currency);
        localStorage.setItem('page', 1);
        $("#main_menu").mainMenu('dropCounts');
        cryts.updated = true;
        setSettingsBtnClass();
        getPageContent(window.location.pathname);
    });

    $("#applySortBtn").click(function () {
        localStorage.setItem('sort_field', cryts.sort_field);
        localStorage.setItem('sort_dir', cryts.sort_dir);
        localStorage.setItem('page', 1);
        $("#main_menu").mainMenu('dropCounts');
        setSortBtnClass();
        getPageContent(window.location.pathname);
    });

    $("#sortClearButton").click(function () {
        localStorage.setItem('sort_field', 'date_public_start');
        localStorage.setItem('sort_dir', 'desc');
        localStorage.setItem('page', 1);
        $("#main_menu").mainMenu('dropCounts');
        setSortBtnClass();
        getPageContent(window.location.pathname);
    });

    $("#crytClearButton").click(function () {
        localStorage.removeItem('per_page')
        localStorage.removeItem('currency')
        localStorage.removeItem('owner_type');
        localStorage.removeItem('filter');
        localStorage.removeItem('page', 1);
        $("#main_menu").mainMenu('dropCounts');
        cryts.updated = true;
        getPageContent(window.location.pathname);
    });

    $(".pagination.per_page a[data-per_page='" + localStorage.getItem('per_page') + "']").parent('li').addClass("active");
    $(".pagination.currency a[data-currency='" + localStorage.getItem('currency') + "']").parent('li').addClass("active");

    $(".pagination.per_page a").click(function () {
        $(".pagination.per_page li").removeClass('active');
        cryts.per_page = $(this).attr('data-per_page');
        $(this).parent('li').addClass("active");
        return false;
    });

    $(".pagination.currency a").click(function () {
        $(".pagination.currency li").removeClass('active');
        cryts.currency = $(this).attr('data-currency');
        $(this).parent('li').addClass("active");
        return false;
    });

    $("#btn_lent").click(function () {
        $('.subtemplate_current').removeClass('btn-primary');
        $('.subtemplate_current i').removeClass('fa-th-list fa-th-large fa-list').addClass('fa-th-list');
        $(".subtemplate_selector li").removeClass('hidden');
        $(".li_" + $(this).attr("data-name")).addClass('hidden');
        $.cookie('subtemplate', 'lent', {expires: 7, path: '/'});
        render_list();
    });
    $("#btn_photo").click(function () {
        $('.subtemplate_current').addClass('btn-primary');
        $('.subtemplate_current i').removeClass('fa-th-list fa-th-large fa-list').addClass('fa-th-large');
        $(".subtemplate_selector li").removeClass('hidden');
        $(".li_" + $(this).attr("data-name")).addClass('hidden');
        $.cookie('subtemplate', 'photo', {expires: 7, path: '/'});
        render_list();
    });
    $("#btn_table").click(function () {
        $('.subtemplate_current').addClass('btn-primary');
        $('.subtemplate_current i').removeClass('fa-th-list fa-th-large fa-list').addClass('fa-list');
        $(".subtemplate_selector li").removeClass('hidden');
        $(".li_" + $(this).attr("data-name")).addClass('hidden');
        $.cookie('subtemplate', 'table', {expires: 7, path: '/'});
        render_list();
    });

    $("a[href='/']").click(function () {
        cryts.searchAllText = '';
        cryts.ff = {};
        localStorage.setItem('page', 1);
        localStorage.setItem('url', $(this).attr("href"));
        getPageContent($(this).attr("href"));
        return false;
    });

    setSettingsBtnClass();
    setSortBtnClass();
    toggleFavorites();
    getPageContent(window.location.pathname);

    $("#home_link").on('mousedown', function(){
        localStorage.setItem('disableSearchString', true);
    });

    if(('interval_reload_top' in window) && (undefined != interval_reload_top)) {
        window.setInterval(function(){

            if(!page_active || $("#map").hasClass('map_fullscreen'))
                return;

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/banners/insert_targeted',
                data: {
                    place: 'top_banner',
                    jsonMode: true,
                    placement: {
                        country: localStorage.getItem('current_country') ? localStorage.getItem('current_country') : false,
                        region: localStorage.getItem('current_region') ? localStorage.getItem('current_region') : false,
                        state: localStorage.getItem('current_state') ? localStorage.getItem('current_state') : false,
                        city: localStorage.getItem('current_city') ? localStorage.getItem('current_city') : false,
                        rajon: localStorage.getItem('current_rajon') ? localStorage.getItem('current_rajon') : false
                    },
                },
                success: function (data) {

                    if(!page_active || $("#map").hasClass('map_fullscreen'))
                        return;
                    if(data.success && data.data != '') {
                        $(".top_banner_container").css('height', $(".top_banner_container").outerHeight() + 'px');
                        $(".top_banner_container").append(data.data);
                        $(".top_banner_container .row:last").css('display', 'none');
                        //$(".top_banner_container .row:last img").ready(function(){
                        $(".top_banner_container .row:first").fadeOut(300, function(){
                            $(".top_banner_container .row:first").remove();
                            $(".top_banner_container .row:last").fadeIn(300);
                            $(".top_banner_container").css('height', 'auto');
                        });
                        //});
                    }
                }
            });
        }, 15000);
    }
}

function showOwnLink() {
    // Обработка ссылки "Только свои" в футере
    if(cryts.owner_type == 'only_own' && cryts.owner == owner_id){
        $('.only_own_link').html('Показать все');
    } else {
        $('.only_own_link').html('Мои объявления');
    };
}

//test
!function ($) {

    $(document).ready(function () {

        page_active = true;

        $(window).blur(function() {
            page_active = false;
        });

        $(window).focus(function() {
            page_active = true;
        });

        // Замена ссылки на выход из истории
        if (window.location.pathname == "/recent/") {
            $("#recent_link").attr("href", "/");
        }

        if($.getUrlVar('buisness_categories')) {
            localStorage.removeItem('go_back_data');
            cryts.ff.buisness_categories = $.getUrlVar('buisness_categories');
            if ($.getUrlVar('business_types'))
                cryts.ff.business_types = $.getUrlVar('business_types');
        }

        initPage();

        localStorage.removeItem('add_from_map-lat');
        localStorage.removeItem('add_from_map-lng');
        localStorage.removeItem('add_from_map-zoom');
        localStorage.removeItem('add_from_map-addr');

        $('.only_own_link').click(function(){
            if(cryts.owner_type == 'only_own' && cryts.owner == owner_id) {
                cryts.owner_type = 'all';
                $("input[type='radio'][name='owner_type'][value='all']").selected(true);
            } else {
                cryts.owner_type = 'only_own';
                cryts.owner = owner_id;
                $("input[type='radio'][name='owner_type'][value='only_own']").selected(true);
            }

            localStorage.setItem('owner_type', cryts.owner_type);
            localStorage.setItem('per_page', cryts.per_page);
            localStorage.setItem('page', 1);
            $("#main_menu").mainMenu('dropCounts');
            cryts.updated = true;
            showOwnLink();
            setSettingsBtnClass();
            getPageContent(window.location.pathname);
        });
    });



}(window.jQuery);
function removeMessagesAlerts() {
    $("#messages .modal-body").children('.alert').remove();
}

(function(){

    $('#messages').on('hidden.bs.modal', function () {
        removeMessagesAlerts();
        $("#messageForm").trigger('reset');
    });

    $("#messageSubmitButton").click(function(){

        removeMessagesAlerts();

        $("#messageForm .wait_screen").show();
        $("#messageForm").ajaxForm({
            dataType: "json",
            success: function(data){
                $("#messageForm .wait_screen").hide();
                if(data.success) {
                    $("#messageForm .modal-body").append('<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + data.reason + '</div>');
                    $("#messageForm").trigger('reset');
                } else {
                    $("#messageForm .modal-body").append('<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + data.reason + '</div>');
                }
            }
        }).submit();
    });

    $("#messageSubmitButton").click(function(){

        $("#messageForm .modal-body .alert").remove();

        $("#messageForm").ajaxForm({
            dataType: "json",
            success: function(data){
                if(data.success) {
                    $("#messageForm .modal-body").append('<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + data.reason + '</div>');
                    $("#messageForm").trigger('reset');
                } else {
                    $("#messageForm .modal-body").append('<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + data.reason + '</div>');
                }
            }
        }).submit();
    });
})(jQuery);


